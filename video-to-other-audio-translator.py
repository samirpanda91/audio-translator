#!/usr/bin/python3
import speech_recognition
from pydub import AudioSegment
from pydub.silence import split_on_silence
from playsound import playsound
from gtts import gTTS
from googletrans import Translator
from moviepy.editor import VideoFileClip

def mp4_to_wav_converter(video_file, audio_file):
    """ Converting Video to Audio """
    videoclip = VideoFileClip(video_file)
    audioclip = videoclip.audio
    audioclip.write_audiofile("output.mp3")
    sound = AudioSegment.from_mp3("output.mp3")
    sound.export(audio_file, format="wav")
    audioclip.close()
    videoclip.close()

def audio_to_text_converter(path="output.wav"):
    """ Converting Audio to Text """
    audio = AudioSegment.from_wav(path)
    file_handler = open("converted.txt", "w+")
    clips = split_on_silence(audio, min_silence_len=900, silence_thresh=-30)
    count = 0
    for clip in clips:
        silent_clip = AudioSegment.silent(duration=10)
        audio_clip = silent_clip + clip + silent_clip

        print("Saving Audio Clip {0}.wav".format(count))
        audio_clip.export("./clip_{0}.wav".format(count), bitrate='192k', format="wav")

        audio_file = 'clip_'+ str(count) + '.wav'
        print("Processing Audio Clip "+ str(count))

        audio_reader = speech_recognition.Recognizer()
        with speech_recognition.AudioFile(audio_file) as source:
            audio_listened = audio_reader.record(source)
        try:
            rec = audio_reader.recognize_google(audio_listened)
            file_handler.write(rec + ". ")
        except speech_recognition.UnknownValueError:
            print("Could Not Process Audio")
        except speech_recognition.RequestError as error:
            print(error)
        count += 1

def english_to_hindi():
    """ English Text to Hindi Test """
    text_content = open('converted.txt', 'r')
    data = text_content.readlines()[0]
    translator = Translator()
    result = translator.translate(data, dest='hi')
    print(result.text)
    return result.text

def play_audio_in_hindi(hindi_text):
    """ Convert English Text to Hindi and Play """
    speak = gTTS(text=hindi_text, lang='hi', slow=False)
    speak.save("converted_hindi_audio.mp3")
    playsound('converted_hindi_audio.mp3')

if __name__ == '__main__':
    VIDEO_FILE = "POC_Python_Developer.mp4"
    AUDIO_FILE = "output.wav"
    mp4_to_wav_converter(VIDEO_FILE, AUDIO_FILE)
    audio_to_text_converter("output.wav")
    hindi_text = english_to_hindi()
    play_audio_in_hindi(hindi_text)

